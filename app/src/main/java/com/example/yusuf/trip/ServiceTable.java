package com.example.yusuf.trip;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "services", foreignKeys = @ForeignKey(entity = TripTable.class, parentColumns = "tid",
        childColumns = "tripID"), indices =  @Index(value = "tripID"))
public class ServiceTable {

    @PrimaryKey(autoGenerate = true)
    private int sid;

    @ColumnInfo(name = "service_name")
    private String name;

    @ColumnInfo(name = "service_cost")
    private double cost;

    @ColumnInfo(name = "service_type")
    private String type;

    //@ColumnInfo(name = "trip_ID")
    private int tripID;


    public int getSid() { return sid; }

    public String getName()
    {
        return name;
    }

    public double getCost()
    {
        return cost;
    }

    public String getType()
    {
        return type;
    }

    public int getTripID()
    {
        return tripID;
    }

    public void setSid(int sid) { this.sid = sid; }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setCost(double cost)
    {
        this.cost = cost;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setTripID(int tripID) { this.tripID = tripID; }
}
