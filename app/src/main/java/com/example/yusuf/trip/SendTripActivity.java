package com.example.yusuf.trip;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.SEND_SMS;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class SendTripActivity extends AppCompatActivity {

    private static final String TAG = "Trip";
    private EditText trip_id;
    //private TextView dest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_trip);


        trip_id = (EditText) findViewById(R.id.snd_trip_src_edtxt);


        Button shareTripButton = findViewById(R.id.share_Trip_Btn);// this button is to share a trip (i.e with all its services)
        shareTripButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                smsShareTripButtonClicked();
            }
        });
    }

    private void smsShareTripButtonClicked()
    {
        if ((ContextCompat.checkSelfPermission(SendTripActivity.this, SEND_SMS) != PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(SendTripActivity.this, READ_CONTACTS) != PERMISSION_GRANTED))
        {
            ActivityCompat.requestPermissions(SendTripActivity.this,
                    new String[] { SEND_SMS, READ_CONTACTS },
                    REQUEST_SEND_SMS);
        }
        else
            {
                pickContact();
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_CONTACT_REQUEST)
        {
            try
            {
                ContentResolver cr = getContentResolver();
                Uri dataUri = data.getData();
                String[] projection = { ContactsContract.Contacts._ID };
                Cursor cursor = cr.query(dataUri, projection, null, null, null);

                if (null != cursor && cursor.moveToFirst())
                {
                    String id = cursor
                            .getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String number = getPhoneNumber(id);
                    if (number == null)
                    {
                        Toast.makeText(getApplicationContext(), "No number in contact", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        TripTable tripTable = ViewAllTrips.tripDatabase.tripTableDao().getTripByID(Integer.parseInt(trip_id.getText().toString()));
                        String tripInString = "";
                        int _id = tripTable.getTid();
                        String _src = tripTable.getSource();
                        String _dest = tripTable.getDestination();
                        tripInString = tripInString+_id+" "+_src+" "+_dest;

                        // here I want to extract a trip and its services from the DB
                        // and then share it via SMS in a particular format that will enable the
                        //recipient device (with same app) extract the trip save it the DB

                        sendSMS(tripInString, number);
                    }
                }
            }
            catch (Exception e)
            {
                Log.e(TAG, e.toString());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(getApplicationContext(), "Permission denied " + requestCode, Toast.LENGTH_SHORT).show();
            return;
        }

        switch(requestCode)
        {
            case REQUEST_SEND_SMS:
                pickContact();
                break;
            default:
                Toast.makeText(getApplicationContext(), "WRONG REQUEST CODE in Permissions", Toast.LENGTH_SHORT).show();
        }
    }

    private static final int REQUEST_SEND_SMS = 10;
    private static final int PICK_CONTACT_REQUEST = 20;

    private void pickContact()
    {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT_REQUEST);
    }

    private String getPhoneNumber(String id)
    {
        ContentResolver cr = getContentResolver();
        String where = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id;
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, where, null, null);

        if (cursor != null && cursor.moveToFirst())
        {
            return cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        return null;
    }

    private void sendSMS(String content, String number)
    {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, content, null, null);
    }

}
