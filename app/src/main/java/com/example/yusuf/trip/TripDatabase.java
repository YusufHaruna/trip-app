package com.example.yusuf.trip;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {TripTable.class, ServiceTable.class}, version = 8, exportSchema = false )
public abstract class TripDatabase extends RoomDatabase {

    public abstract TripTableDao tripTableDao();
    public abstract ServiceTableDao serviceTableDao();

    private static TripDatabase INSTANCE;

    public static TripDatabase getDatabase(Context con) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(con.getApplicationContext(), TripDatabase.class, "trip.db").allowMainThreadQueries()
                            .fallbackToDestructiveMigration().build();
        }
        return INSTANCE;
    }
}
