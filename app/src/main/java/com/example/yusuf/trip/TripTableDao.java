package com.example.yusuf.trip;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface TripTableDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertTrip(TripTable tripTable);

    @Query("Select * from trips where trip_source =:source & trip_destination =:destination")
    public TripTable getTrip(String source, String destination);

    @Query("SELECT * FROM trips")
    LiveData<List<TripTable>> getAllTrips();
     //List<TripTable> getAllTrips();

    @Query("Select * from trips where tid =:id")
    public TripTable getTripByID(int id);


    //@Update

    //@Delete



}
