package com.example.yusuf.trip;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button createTripButton = (Button) findViewById(R.id.btn_create_trip);
        createTripButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateTripActivity.class);
                startActivity(intent);
            }
        });

        Button viewTripsButton = (Button) findViewById(R.id.btn_view_trips);
        viewTripsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ViewAllTrips.class);
                startActivity(intent);
            }
        });

        Button sendTripButton = (Button) findViewById(R.id.btn_send_trip);
        sendTripButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SendTripActivity.class);
                startActivity(intent);
            }
        });


    }


    /*public void createTrip(View view)
    {
        Intent intent = new Intent(this, CreateTripActivity.class);
        startActivity(intent);
    }


    public void viewTrip(View view)
    {
        Intent intent = new Intent(this, ViewAllTrips.class);
        startActivity(intent);
    }



    public void sendTrip(View view)
    {
        Intent intent = new Intent(this, SendTripActivity.class);
        startActivity(intent);
    }
*/
}
