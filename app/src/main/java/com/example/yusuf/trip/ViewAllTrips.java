package com.example.yusuf.trip;

import android.arch.lifecycle.LiveData;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ViewAllTrips extends AppCompatActivity {

    private TextView trip_txt_View;
    public static TripDatabase tripDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_trips);

        trip_txt_View = findViewById(R.id.txt_view_trip);
        tripDatabase = TripDatabase.getDatabase(getApplicationContext());

        //ArrayList<TripTable> trips = null;
        LiveData<List<TripTable>> trips = null;
        try
        {
            trips = ViewAllTrips.tripDatabase.tripTableDao().getAllTrips();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // I am getting the problem of foreach loop
        String trip_info = "";
        for (TripTable trp : trips)
        {
            int _id = trp.getTid();
            String _src = trp.getSource();
            String _des = trp.getDestination();

            trip_info = trip_info+"\n\n"+"Id: "+_id+"\n"+"source: "+_src+"\n"+"Destination: "+_des;
        }
        trip_txt_View.setText(trip_info);
    }
}
