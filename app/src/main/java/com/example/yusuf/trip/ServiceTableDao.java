package com.example.yusuf.trip;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface ServiceTableDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertService(ServiceTable serviceTable);

    //@Update


    //@Query()

    //@Delete
}
