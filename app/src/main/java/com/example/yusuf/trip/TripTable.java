package com.example.yusuf.trip;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "trips", indices = {@Index(value = {"trip_source","trip_destination"}, unique = true)})
public class TripTable {

    @PrimaryKey(autoGenerate = true)
    private int tid;

    @ColumnInfo(name = "trip_source")
    private String source;

    @ColumnInfo(name = "trip_destination")
    private String destination;


    public int getTid() { return tid; }

    public String getSource() { return source; }

    public String getDestination() { return destination; }

    public void setTid(int tid) { this.tid = tid; }

    public void setSource(String source) { this.source = source; }

    public void setDestination(String destination) { this.destination = destination; }
}
