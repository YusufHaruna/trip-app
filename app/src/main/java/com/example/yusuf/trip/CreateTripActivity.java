package com.example.yusuf.trip;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateTripActivity extends AppCompatActivity
{

    //private TripDatabase myTripDatabase;
    //private EditText editSource;
    //private EditText editDestination;

    public TripDatabase myTripDatabase;
    public EditText editSource;
    public EditText editDestination;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_trip);

        myTripDatabase = TripDatabase.getDatabase(getApplicationContext());

        editSource = (EditText) findViewById(R.id.edTxt_createTrip_src);
        editDestination = (EditText) findViewById(R.id.edTxt_creareTrip_des);


        Button createTrip_Btn = (Button) findViewById(R.id.btn2_create_trip);
        createTrip_Btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (editSource.getText().length() == 0)
            {
                Toast.makeText(getApplicationContext(), "Enter a source", Toast.LENGTH_LONG);
                return;
            }

                if (editDestination.getText().length() == 0)
                {
                    Toast.makeText(getApplicationContext(), "Enter a destination", Toast.LENGTH_LONG);
                    return;
                }

                TripTable trip = new TripTable();

                String src = editSource.getText().toString();
                String dest = editDestination.getText().toString();

                trip.setSource(src);
                trip.setDestination(dest);

                //MainActivity.myTripDatabase.tripTableDao().insertTrip(trip);

                //myTripDatabase.tripTableDao().insertTrip(trip);
                //Toast.makeText(getApplicationContext(), "Trip added", Toast.LENGTH_SHORT).show();

                editSource.setText("");
                editDestination.setText("");

                myTripDatabase.tripTableDao().insertTrip(trip);
                //TripTable id_trip = myTripDatabase.tripTableDao().getTrip(trip.getSource(), trip.getDestination());
                Toast.makeText(getApplicationContext(), "Trip added successfully with ID "/*+id_trip.getTid()*/, Toast.LENGTH_SHORT).show();}
        });
    }


    public void addService(View view)
    {
        Intent intent = new Intent(this, AddServiceActivity.class);
        startActivity(intent);
    }
}
