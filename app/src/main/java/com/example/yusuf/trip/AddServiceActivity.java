package com.example.yusuf.trip;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddServiceActivity extends AppCompatActivity
{

    //private TripDatabase myTripDatabase2;
    //private EditText editName;
    //private EditText editCost;
    //private EditText editType;
    //private EditText editTripId;

    public TripDatabase myTripDatabase;
    public EditText editName;
    public EditText editCost;
    public EditText editType;
    public EditText editTripId;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);

        myTripDatabase = TripDatabase.getDatabase(getApplicationContext());

        editName = (EditText) findViewById(R.id.editText_serName);
        editCost = (EditText)findViewById(R.id.editText_serCost);
        editType = (EditText)findViewById(R.id.editText_serType);
        editTripId = (EditText)findViewById(R.id.editText_tripID);

        Button save_service_Btn = findViewById(R.id.btn_saveService);
        save_service_Btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String serName = editName.getText().toString();
                String serCost = editCost.getText().toString();
                String serType = editType.getText().toString();
                String serTrip_ID = editTripId.getText().toString();

                ServiceTable service = new ServiceTable();
                service.setName(serName);
                service.setCost(Double.parseDouble(serCost));
                service.setTripID(Integer.parseInt(serTrip_ID));

                editName.setText("");
                editCost.setText("");
                editType.setText("");
                editTripId.setText("");

                myTripDatabase.serviceTableDao().insertService(service);
                Toast.makeText(getApplicationContext(), "service added successfully", Toast.LENGTH_LONG).show();


            }
        });
    }
}
